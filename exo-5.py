import dash
from dash import html, dcc
from dash.dependencies import Output, Input
import pandas as pd
import plotly.express as px
import function  # Assuming function.py is in the same directory

# Load the data using the provided function
df = function.json_file('data-avion.json')

# Initialize the Dash app
app = dash.Dash(__name__)

# App layout
app.layout = html.Div([
    html.H1("Exercise 5 Content"),
    html.H1("Destination Distribution", style={'textAlign':'center'}),
    dcc.Graph(id='destination-pie-chart'),  # Now dcc is defined
])

# Callback to update the pie chart based on the destination distribution
@app.callback(
    Output('destination-pie-chart', 'figure'),
    [Input('destination-pie-chart', 'id')]  # Empty Input since it doesn't depend on specific user input
)
def update_pie_chart(_):
    # Calculate the distribution of destinations
    destination_distribution = df['Destination'].value_counts()

    # Create a pie chart using Plotly Express
    fig = px.pie(
        destination_distribution,
        names=destination_distribution.index,
        values=destination_distribution.values,
    )

    return fig

if __name__ == '__main__':
    # Run the Dash app
    app.run_server(debug=True)
