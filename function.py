import pandas as pd

def json_file(url):
    df1 = pd.read_json(url)
    
    # Convert date and time columns to human-readable format
    df1['Date_of_Journey'] = pd.to_datetime(df1['Date_of_Journey'], unit='ms').dt.strftime('%Y-%m-%d')
    df1['Dep_Time'] = pd.to_datetime(df1['Dep_Time'], unit='ms').dt.strftime('%H:%M:%S')
    df1['Arrival_Date'] = pd.to_datetime(df1['Arrival_Date'], unit='ms').dt.strftime('%Y-%m-%d')
    df1['Duration'] = pd.to_datetime(df1['Duration'], unit='ms').dt.strftime('%H:%M:%S')
    df1['Arrival_Time'] = pd.to_datetime(df1['Arrival_Time'], unit='ms').dt.strftime('%H:%M:%S')
    df1['Airline'] = df1['Airline'].astype(str)
    df1['Source'] = df1['Source'].astype(str)
    df1['Destination'] = df1['Destination'].astype(str)
    df1['Route'] = df1['Route'].str.split(' → ')
    return df1