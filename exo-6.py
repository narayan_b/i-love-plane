import dash
from dash import html, dcc
from dash.dependencies import Output, Input
import pandas as pd
import plotly.express as px
import function  # Assuming function.py is in the same directory

# Load the data using the provided function
df = function.json_file('data-avion.json')

# Initialize the Dash app
app = dash.Dash(__name__)

# App layout
app.layout = html.Div([
    html.H1("Exercise 6 Content"),
    html.H1("Source Distribution", style={'textAlign':'center'}),
    dcc.Graph(id='Source-pie-chart'), 
])

# Callback to update the pie chart based on the Source distribution
@app.callback(
    Output('Source-pie-chart', 'figure'),
    [Input('Source-pie-chart', 'id')] 
)
def update_pie_chart(_):
    # Calculate the distribution of Sources
    Source_distribution = df['Source'].value_counts()

    # Create a pie chart using Plotly Express
    fig = px.pie(
        Source_distribution,
        names=Source_distribution.index,
        values=Source_distribution.values,
    )

    return fig

if __name__ == '__main__':
    # Run the Dash app
    app.run_server(debug=True)
