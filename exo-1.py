# Import packages
from dash import Dash, html, dash_table
import pandas as pd
import function 

# Initialize the app
app = Dash(__name__)
df1 = function.json_file('data-avion.json')
df1['Route'] = df1['Route'].astype(str)
# App layout
app.layout = html.Div([
    html.H1("Exercise 1 Content"),
    html.H1(children='A filterable and sorted table that displays all the information.', style={'textAlign':'center'}),
    dash_table.DataTable(
        id='datatable',
        columns=[{'name': col, 'id': col} for col in df1.columns],
        data=df1.to_dict('records'),
        page_size=30,

        # Enable filtering
        filter_action='native',
        sort_action='native',
        virtualization=True,
    ),
])

# Run the app
if __name__ == '__main__':
    app.run(debug=True)
