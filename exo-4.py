import dash
from dash import dcc, html
from dash.dependencies import Input, Output
from function import json_file
import plotly.express as px

df = json_file('data-avion.json')

app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Exercise 4 Content"),
    html.H1("All Cities Sorted by Expensive to Cheap Price"),

    html.Ul(id='output-container1'),

    dcc.Graph(id='price-histogram'),

    html.Hr(),

    html.H1("Destination by Source"),
    dcc.Dropdown(
        id='source-dropdown',
        options=[{'label': source, 'value': source} for source in df['Source'].unique()],
        value=df['Source'].unique()[0],
        multi=False
    ),
    html.Ul(id='output-container2')
])

@app.callback(
    [Output('output-container1', 'children'),
     Output('price-histogram', 'figure')], 
    [Input('output-container1', 'children')] 
)
def update_output_all_cities(dummy_input):
    # Calculate the average price for each destination
    avg_prices = df.groupby('Destination')['Price'].mean().sort_values(ascending=False)

    # Create a list of destinations sorted by average price
    sorted_destinations = list(avg_prices.index)

    # Create a list of list items for the output
    list_items = [html.Li(dest) for dest in sorted_destinations]

    # Create a histogram using Plotly Express based on average prices for each destination
    fig = px.bar(x=sorted_destinations, y=avg_prices, labels={'x': 'Destination', 'y': 'Average Price'},
                 title='Average Price Distribution of All Cities')


    return list_items, fig

@app.callback(
    Output('output-container2', 'children'),
    [Input('source-dropdown', 'value')]
)
def update_output_destination_by_source(selected_source):
    if selected_source is None:
        return "Select your city"

    # Filter the dataframe based on the selected source
    filtered_df = df[df['Source'] == selected_source]

    # Calculate the average price for each destination
    avg_prices = filtered_df.groupby('Destination')['Price'].mean().sort_values(ascending=False)

    # Create a list of destinations sorted by average price
    sorted_destinations = list(avg_prices.index)

    # Create a list of list items for the output
    list_items = [html.Li(dest) for dest in sorted_destinations]

    return list_items


if __name__ == '__main__':
    app.run_server(debug=True)
