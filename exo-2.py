import dash
from dash import html, dash_table, dcc
from dash.dependencies import Output, Input
import pandas as pd
import function

# Load the data using the provided function
df = function.json_file('data-avion.json')

# Get all unique airlines for the initial dropdown options
all_airlines = df['Airline'].unique()

app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("Exercise 2 Content"),
    html.H1(children='A table for each Airline with a list of cities they serve.', style={'textAlign': 'center'}),
    
    # Add a dropdown for selecting airlines
    dcc.Dropdown(
        id='airline-dropdown',
        options=[{'label': airline, 'value': airline} for airline in all_airlines],
        multi=False,
        placeholder='Select an Airline',
        style={'width': '500px', 'maxWidth': '90%', 'margin': 'auto'}  
    ),
    
    dash_table.DataTable(
        id='table-output',
        columns=[
            {'name': 'City', 'id': 'City'},
            {'name': 'City_Code', 'id': 'City_Code'}
        ],
        page_size=30,
        # Remove filter and sort actions
        filter_action='none',
        sort_action='none',
        virtualization=True,
        style_table={'width': '500px', 'maxWidth': '90%', 'margin': 'auto'},
        style_header={'backgroundColor': 'black', 'color': 'white', 'textAlign': 'center'},
        style_data={'backgroundColor': 'lightgray', 'whiteSpace': 'normal'},
        style_data_conditional=[
            {
                'if': {'column_id': 'City'},
                'textAlign': 'center',
            },
            {
                'if': {'column_id': 'City_Code'},
                'textAlign': 'center',
            },
        ],
    ),
])

@app.callback(
    [Output('table-output', 'data'),
     Output('airline-dropdown', 'options')],
    [Input('airline-dropdown', 'value')]
)
def update_table(selected_airline):
    # If no airline selected, show all data and include all airlines in dropdown
    if selected_airline is None:
        return df[['Source', 'Destination']].to_dict('records'), [{'label': airline, 'value': airline} for airline in all_airlines]

    # Filter data based on the selected airline
    filtered_df = df[df['Airline'] == selected_airline]

    # Create a DataFrame for the city and city code mapping
    city_mapping_df = pd.DataFrame(columns=['City', 'City_Code'])

    # Extract unique cities from Source and Destination columns
    unique_cities = set(filtered_df['Source'].unique()).union(set(filtered_df['Destination'].unique()))

    # Populate the city_mapping_df DataFrame with unique cities
    city_mapping_df['City'] = list(unique_cities)

    # Create a mapping dictionary from Route column
    city_code_mapping = {}
    for _, row in filtered_df.iterrows():
        route = row['Route']
        if isinstance(route, list) and len(route) >= 2:
            city_code_mapping[row['Source']] = route[0]
            city_code_mapping[row['Destination']] = route[-1]
        else:
            city_code_mapping[row['Source']] = None
            city_code_mapping[row['Destination']] = None

    # Use the mapping to populate the City_Code column
    city_mapping_df['City_Code'] = [city_code_mapping[city] for city in city_mapping_df['City']]

    # Return data without the 'Route' column and updated dropdown options
    return city_mapping_df[['City', 'City_Code']].to_dict('records'), [{'label': airline, 'value': airline} for airline in all_airlines]

if __name__ == "__main__":
    # Run the Dash app
    app.run_server(debug=True)
