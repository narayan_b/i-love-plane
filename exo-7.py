# import dash
# from dash import dcc, html
# from dash.dependencies import Input, Output
# import plotly.express as px
# import pandas as pd
# import function

# # Load data
# df = function.json_file("data-avion.json")

# # Create a list to store all connecting points
# all_points = []

# # Iterate through each row in the "Route" column
# for route_list in df['Route']:
#     # Check if route_list is None
#     if route_list is not None:
#         # Extend the list with the connecting points from the current route
#         all_points.extend(route_list)

# # Create a DataFrame from the list of connecting points
# df_points = pd.DataFrame({'Airport': all_points})

# # Create Dash app
# app = dash.Dash(__name__)

# # Layout of the app
# app.layout = html.Div([
#     html.H1("Connecting Points Histogram"),
#     dcc.Graph(id='histogram'),
# ])

# # Callback to update histogram based on user input
# @app.callback(
#     Output('histogram', 'figure'),
#     [Input('histogram', 'id')] 
# )
# def update_histogram(value):
#     # Create a histogram using Plotly Express
#     fig = px.histogram(df_points, x='Airport', title='Connecting Points Histogram')
#     return fig

# # Run the app
# if __name__ == '__main__':
#     app.run_server(debug=True)


############################################################################################
############################################################################################
############################################################################################



import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd
import function

# Load data
df = function.json_file("data-avion.json")

# Handle None values in the 'Route' column
df['intermediate_stops'] = df['Route'].apply(lambda x: x[1:-1] if x and len(x) > 2 else [])

# Explode and drop NaN values to get a flat list of intermediate airports
intermediate_airports = df['intermediate_stops'].explode().dropna()

# Count the occurrences of each intermediate airport
intermediate_counts = intermediate_airports.value_counts()

# Create a DataFrame from the counts
df_intermediate_counts = intermediate_counts.reset_index()
df_intermediate_counts.columns = ['Airport', 'Number_of_Connections']

# Create Dash app
app = dash.Dash(__name__)

# Layout of the app
app.layout = html.Div([
    html.H1("Exercise 7 Content"),
    html.H1("Connecting Points Histogram", style={'textAlign': 'center'}),
    dcc.Graph(id='histogram'),
])

# Callback to update histogram based on user input
@app.callback(
    Output('histogram', 'figure'),
    [Input('histogram', 'id')]
)
def update_histogram(value):
    # Create a histogram using Plotly Express
    fig = px.histogram(df_intermediate_counts, x='Airport', y='Number_of_Connections')
    return fig

# Run the app
if __name__ == '__main__':
    app.run_server(debug=True)
