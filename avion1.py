import datetime
import pandas as pd

excel_file = 'data-avion.xlsx'
df = pd.read_excel(excel_file, sheet_name='Sheet1')

df['Date_of_Journey'] = pd.to_datetime(df['Date_of_Journey'], format='%d/%m/%Y')
df['Dep_Time'] = pd.to_timedelta(df['Dep_Time']+':00')
df['Duration'] = pd.to_timedelta(df['Duration'])
df['Arrival_Time'] = df['Date_of_Journey'] + df['Dep_Time'] + df['Duration']

df['Arrival_Date'] = df['Arrival_Time'].dt.date
df['Arrival_Time'] = df['Arrival_Time'].dt.time

print(df[['Date_of_Journey', 'Dep_Time', 'Duration', 'Arrival_Date', 'Arrival_Time']])

output_file_path = 'data-avion.json' 
df.to_json(output_file_path, index=False)
