import pandas as pd
from dash import Dash, html, dcc, Input, Output, dash_table
import function

app = Dash(__name__)
df = function.json_file("data-avion.json")

# Supposons que votre DataFrame s'appelle df et la colonne temps_colonne
# Convertir la colonne temps_colonne en timedelta
df['Duration'] = pd.to_timedelta(df['Duration'])

# Extraire les minutes de la colonne timedelta
df['Duration_en_minutes'] = df['Duration'].dt.total_seconds() / 60

# Afficher le résultat
# print(df[['Airline', 'Duration', 'Duration_en_minutes']])

# Create a list of unique cities for the dropdown options
city_options = [{'label': city, 'value': city} for city in df['Source'].unique()]
city_options.extend([{'label': city, 'value': city} for city in df['Destination'].unique()])

app.layout = html.Div([
    html.H1("Exercise 3 Content"),
    html.H1(children='A view with 2 selects.', style={'textAlign': 'center'}),
    html.H4(
        children='A view with 2 selects. In each list of airports. When I select both, display the average time and the average price of a journey between the two.'),

    dcc.Dropdown(
        id='source-dropdown',
        options=city_options,
        multi=False,
        value=0,
        style={'width': '48%', 'display': 'inline-block'}
    ),

    dcc.Dropdown(
        id='destination-dropdown',
        options=city_options,
        multi=False,
        value=0,
        style={'width': '48%', 'display': 'inline-block'}
    ),

    dash_table.DataTable(
        id='datatable',
        columns=[
            {'name': 'Average Duration', 'id': 'Average Duration'},
            {'name': 'Average Price', 'id': 'Average Price'}
        ],
        page_size=30,
    )
])

@app.callback(
    Output('datatable', 'data'),
    Input('source-dropdown', 'value'),
    Input('destination-dropdown', 'value')
)
def update_table(selected_source, selected_destination):
    # print(f"Selected Source: {selected_source}, Selected Destination: {selected_destination}")

    # If neither source nor destination is selected, display the original table
    avg_data = {'Average Duration': '', 'Average Price': ''}
    if selected_source is None or selected_destination is None:
        return [avg_data]

    # Filter the dataframe based on selected source and destination
    filtered_df = df[(df['Source'] == selected_source) & (df['Destination'] == selected_destination)]

    if filtered_df.empty:
        # If no data is found for the selected source and destination, display the original table
        avg_data = {'Average Duration': '', 'Average Price': ''}
    else:
        # Calculate average time and average price for the selected journey
        avg_duration = filtered_df['Duration_en_minutes'].mean()

        avg_price = filtered_df['Price'].mean()

        # Create a dictionary with the average values
        avg_data = {'Average Duration': avg_duration, 'Average Price': avg_price}

    print(f"Filtered Data: {avg_data}")
    return [avg_data]


if __name__ == '__main__':
    app.run_server(debug=True)

