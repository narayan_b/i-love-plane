# import dash
# from dash import dcc, html
# from dash.dependencies import Input, Output
# import plotly.express as px
# import pandas as pd
# import function

# # Load data
# df = function.json_file("data-avion.json")

# # Assuming 'Duration' column is in datetime format, you can convert it if needed
# # df['Duration'] = pd.to_datetime(df['Duration'])

# # Create a Dash app
# app = dash.Dash(__name__)

# # Create a layout for the app
# app.layout = html.Div([
#     html.H1("Journey Durations Histogram"),
#     dcc.Graph(id='duration-histogram'),
# ])

# # Callback to update histogram based on user input
# @app.callback(
#     Output('duration-histogram', 'figure'),
#     [Input('duration-histogram', 'id')]  # Change 'value' to 'id'
# )
# def update_histogram(value):
#     # Create a histogram using Plotly Express
#     fig = px.histogram(df, x='Duration', title='Journey Durations Histogram',
#                        labels={'Duration': 'Duration (in hours)'},
#                        nbins=20,  # Adjust the number of bins as needed
#                        marginal='box',  # Add box plot for additional insights
#                        hover_data=['Route'])  # Show route information on hover
    
#     # Add mean and max lines to the plot
#     fig.update_layout(
#         shapes=[
#             dict(
#                 type='line',
#                 yref='y',
#                 y0=0,
#                 y1=1,
#                 xref='x',
#                 x0=df['Duration'].mean(),
#                 x1=df['Duration'].mean(),
#                 line=dict(color='red', width=2),
#                 name='Mean Duration',
#             ),
#             dict(
#                 type='line',
#                 yref='y',
#                 y0=0,
#                 y1=1,
#                 xref='x',
#                 x0=df['Duration'].max(),
#                 x1=df['Duration'].max(),
#                 line=dict(color='green', width=2),
#                 name='Max Duration',
#             ),
#         ],
#     )

#     return fig

# # Run the app
# if __name__ == '__main__':
#     app.run_server(debug=True)



##################################################################

import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import dash_table
import pandas as pd
import function

# Load data
df = function.json_file("data-avion.json")

# Convert 'Duration' to datetime format
df['Duration'] = pd.to_datetime(df['Duration'], format='%H:%M:%S').dt.time

# Calculate the maximum, minimum, and mean duration
max_duration = df['Duration'].max()
min_duration = df['Duration'].min()

# Convert time to seconds for mean calculation
df['Duration_seconds'] = df['Duration'].apply(lambda x: x.hour * 3600 + x.minute * 60 + x.second)

# Calculate mean duration in seconds and convert back to time format
mean_duration_seconds = df['Duration_seconds'].mean()
mean_duration = pd.to_datetime(mean_duration_seconds, unit='s').time()

# Convert 'Route' to a string
df['Route'] = df['Route'].apply(lambda route_list: ' → '.join(route_list) if route_list else '')

# Create Dash app
app = dash.Dash(__name__)

# Layout of the app
app.layout = html.Div([
    html.H1("Exercise 8 Content"),
    html.H1("Flight Duration Statistics"),
    html.Div([
        html.P(f"Maximum Duration: {max_duration.strftime('%H:%M:%S')}"),
        html.P(f"Minimum Duration: {min_duration.strftime('%H:%M:%S')}"),
        html.P(f"Mean Duration: {mean_duration.strftime('%H:%M:%S')}"),
    ]),
    html.H2("Flight Details Table"),
    dash_table.DataTable(
        id='flight-details-table',
        columns=[
            {"name": col, "id": col} for col in ['Airline', 'Source', 'Destination', 'Route', 'Duration']
        ],
        data=df[['Airline', 'Source', 'Destination', 'Route', 'Duration']].to_dict('records'),
    ),
])
# Run the app
if __name__ == '__main__':
    app.run_server(debug=True)
