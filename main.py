import dash
from dash import dcc, html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

# Initialize Dash app
app = dash.Dash(__name__)

# Layout of the main app
app.layout = html.Div([
    html.H1("Choose an Exercise"),
    dcc.Link(html.Button("Exercise 1"), href="/exo-1"),
    dcc.Link(html.Button("Exercise 2"), href="/exo-2"),
    dcc.Link(html.Button("Exercise 3"), href="/exo-3"),
    dcc.Link(html.Button("Exercise 4"), href="/exo-4"),
    dcc.Link(html.Button("Exercise 5"), href="/exo-5"),
    dcc.Link(html.Button("Exercise 6"), href="/exo-6"),
    dcc.Link(html.Button("Exercise 7"), href="/exo-7"),
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content'),
])

# Callback to update page content based on URL
@app.callback(
    Output('page-content', 'children'),
    Input('url', 'pathname')
)
def display_page(pathname):
    try:
        # Dynamically import and execute the requested exercise app
        module_name = pathname[1:]
        module = __import__(module_name, fromlist=['app'])
        return module.app.layout
    except ImportError as e:
        print(f"Import error for module {module_name}: {e}")
        raise PreventUpdate

# Run the main app
if __name__ == '__main__':
    app.run_server(debug=True)